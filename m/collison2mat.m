function data = collison2mat(folder,fnm)
% function data = collison2mat(folder,fnm)
%
% Parses the SRIM output file COLLISON.txt from the specified folder and
% returns all data in a matrix.
%
% Only files created with the SRIM option "Kinchin-Pease" are supported.
%
% If the original file has been renamed, pass the new name in fnm
%
% Each row of the output matrix data corresponds to an ion collision event.
% The 9 columns store the following information:
%   1 :  ion number
%   2 :  ion energy (keV)
%   3 :  Depth (X) (Angstroem)
%   4 :  Lateral distance (Y) (Angstroem)
%   5 :  Lateral distance (Z) (Angstroem)
%   6 :  Electronic stopping Se (eV/Angstroem)
%   7 :  Atomic number of recoil atom
%   8 :  Recoil energy (eV)
%   9 :  Target displacements
%

% Check input
if ~isfolder(folder)
  error('%s is not a folder');
endif

if nargin < 2
  fnm = 'COLLISON.txt';
endif

fname = file_in_path(folder,fnm);
if isempty(fname)
  error('file %s not found in %s',fnm,folder);
end

% Special SRIM char = 0xB3 used as data separator
sc = char(11*16+3); % 0xB3 char
sc = unicode2native(sc,'ascii'); % convert to ascii code
sc = char(sc); % convert to ascii char

% open file as text with ascii encoding
% note:
%   ascii encoding needed in order to convert special char 0xB3
%   to a single byte (char(63))
%   Otherwise 0xB3 may be converted to a double-byte unicode value
%
fid = fopen(fname,'rt',native_float_format (),'ascii');
% temporary storage file
ftmp = fopen(tempname,'w+');

info = stat(fid);
estnlines = ceil(info.size / 100);



% line counter
k = 0;

# skip header up to an empty line
while ~feof(fid)
    line = fgetl(fid);
    k++;
    ##disp(sprintf('%d. %s|',k,line))
    if strcmp(line, " ")
      break
    endif
end

# go to 1st data line
while ~feof(fid)
    line = fgetl(fid);
    k++;
    ##disp(sprintf('%d. %s|',k,line))
    if ~isempty(line) && line(1)==sc,
      break
    endif
end

# check for full cascades option
if ~isempty(strfind(line,'<== Start of New Cascade'))
  fclose(fid);
  fclose(ftmp);
  error('Full cascades not supported');
end

# 1st pass, count data lines
disp(sprintf('Reading file %s',fname))

emap = elmnts_map;

while ~feof(fid)
  line = fgetl(fid);
  if isempty(line) || line(1)~=sc
    continue
  else
    k++;
    d = parse_data_line(line,emap,sc);
    % if any(isnan(d));
    fwrite(ftmp,d,'double');
    if mod(k,ceil(estnlines/10))==0,
      fprintf(stdout,'...%d%%',round(k*100/estnlines));
      fflush(stdout);
    end
  endif
end
fprintf(stdout,'...100%%\n');
fflush(stdout);
fclose(fid);

fseek(ftmp,0,'bof');
data = fread(ftmp,Inf,"double");
fclose(ftmp);
n = length(data);
data = reshape(data,9,n/9).';

end

function d = parse_data_line(line,emap,sc)
  fmtstr = [sc '%d' sc '%g' sc '%g' sc '%g' ...
            sc '%g' sc '%g' sc ' %s ' sc '%g' sc '%g' sc];
  d = ones(1,9)*NaN;
  [d(1),d(2),d(3),d(4),d(5),d(6),s,d(8),d(9)] = sscanf(line,fmtstr,'C');
  d(7) = emap(strtrim(s));
end

function r = check_data_line(line,emap,sc)
  d = parse_data_line(line,emap,sc);
  r = ~any(isnan(d));
end

function emap = elmnts_map()
  emap = containers.Map({"H","He","Li","Be","B","C","N","O","F","Ne",...
              "Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca",...
              "Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn",...
              "Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y","Zr",...
              "Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn",...
              "Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd",...
              "Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb",...
              "Lu","Hf","Ta","W","Re","Os","Ir","Pt","Au","Hg",...
              "Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th",...
              "Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm",...
              "Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds",...
              "Rg","Cn","Nh","Fl","Mc","Lv","Ts","Og"},1:118);
end




