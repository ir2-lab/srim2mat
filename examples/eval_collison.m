%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Load SRIM COLLISON.txt data and 
% generate some statistics & plots
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
addpath('../m');

testcases = { ...
  '1 MeV H ions on Fe target',...
  '5 MeV H ions on Fe target',...
  '2 MeV Fe ions on Fe target'...
  '78 keV Fe ions on Fe target'};
  
testfolders = { ...
  './1MeV_H_on_Fe', ...
  './5MeV_H_on_Fe', ...
  './2MeV_Fe_on_Fe',...
  './78keV_Fe_on_Fe'};
  
opt = menu('Choose SRIM test case', testcases);
if opt==0, return; end

# Read collision data 
# tic
# data = collison2mat(testfolders{opt});
# toc

## Parsing COLLISON.txt can be a lengthy operation
## Saving the parsed data to a binary file makes quick reuse easier, e.g.:
## > save collison.dat data 
## We have done that for all testcases.
## Thus we read here directly the binary file
load([testfolders{opt} '/collison.dat'])

titlestr = testcases{opt};

% totals
Nevents = size(data,1); % scattering events
Nions = data(end,1); % ion histories

% 1. PKA spectrum
T=data(:,8); % recoil energy
Ed=40; % eV
x = log10(T/Ed);
xb = linspace(0,3,51);
h = histc(x,xb);
Tb = 10.^xb*Ed;
hs = h(1:end-1)'./diff(Tb)/Nevents;
Tx = 0.5*(Tb(1:end-1)+Tb(2:end));
i = find(hs>0);
hs = hs(i);
Tx = Tx(i);
figure 1
clf
loglog(Tx,hs,'.-',Tx,Tx.^(-2)*hs(3)*Tx(3)^2)
xlabel('PKA recoil energy T (eV)')
ylabel('P(T) (eV^{-1})')
title([titlestr ' - PKA spectrum'])
legend('SRIM', 'P(T)\propto T^{-2}')

% 2. Probability for n collisions of an ion : pcol(n=0,1,2,3,...10)
pcol = zeros(1,100);
i = data(1,1); % first ion number
n = 1; % collisions for this ion
N0 = 0; % ions with no-collision
for k=2:Nevents,
  if data(k,1)==i,
    n++;
  else
    if n<=100, pcol(n)++;; end
    n=1;
    N0 += data(k,1) - i - 1;
    i=data(k,1);
  endif
end

i = find(pcol>0);
i = i(end);
pcol=pcol(1:i);
pcol = [N0 pcol];
pcol = pcol/sum(pcol);

figure 2
clf
bar(0:i,pcol)
xlabel('n ion collisions')
ylabel('p(n)')
title([titlestr ' - Probability for ion collisions'])

% 3. Lateral PKA distribution
figure 3
r = sqrt(data(:,4).^2+data(:,5).^2)/10; % range in nm
rb = linspace(0,5*mean(r),51);
h = histc(r,rb)';
h = h(1:end-1)/pi./diff(rb.^2)/Nevents;
bar(rb(1:end-1),h*pi.*rb(2:end).^2,"histc")
xlabel('r (nm)')
ylabel('\pi r^2 \rho(r)')
title([titlestr ' - Lateral PKA distribution'])

