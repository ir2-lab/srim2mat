%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Process all COLLISON.txt files in the 
% test case folders and save to binary format
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear
addpath('../m');

testcases = { ...
  '1 MeV H ions on Fe target',...
  '5 MeV H ions on Fe target',...
  '2 MeV Fe ions on Fe target'...
  '78 keV Fe ions on Fe target'};
  
testfolders = { ...
  './1MeV_H_on_Fe', ...
  './5MeV_H_on_Fe', ...
  './2MeV_Fe_on_Fe',...
  './78keV_Fe_on_Fe'};

for i=1:4,
  disp(['Proccesing data in ' testfolders{i} ' ...'])
  tic
  data = collison2mat(testfolders{i});
  toc
  save -binary collison.dat data
  movefile('collison.dat', testfolders{i});
  disp('Done.')
  disp('')
end
