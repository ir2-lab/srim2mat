# # Using SRIM to calculate displacements according to arc-dpa 
# 
clear % clear all data 
addpath('../m'); % to access srim2mat routines
testcases = { ...
  '1 MeV H ions on Fe target',...
  '5 MeV H ions on Fe target',...
  '2 MeV Fe ions on Fe target'...
  '78 keV Fe ions on Fe target'};
  
testfolders = { ...
  './1MeV_H_on_Fe', ...
  './5MeV_H_on_Fe', ...
  './2MeV_Fe_on_Fe',...
  './78keV_Fe_on_Fe'};

opt = menu('Choose SRIM test case', testcases);
if opt==0, return; end
folder = testfolders{opt};
% load SRIM txt files
A = srim2mat(folder);
% load processed COLISSON data (generates the variable "data")
load([folder '/collison.dat']);
T = data(:,8);
v = data(:,9);
figure 1
clf
loglog(T,v,'.')
xlabel('PKA Recoil Energy T (eV)')
ylabel('# vacancies, \nu')
title(testcases{opt})

disp('Average displacements per PKA, <nu>')
disp(['from VACANCY.txt  : ', num2str(sum(A.Vi+A.Vr)/sum(A.Vi),4)])
Ni = data(end,1); % # of ions
disp(['from COLLISON.txt : ', num2str(mean(v),4)])

Tdam = v*100; % "reverse engineered" damage energy (eV)

% Lindhard approx from Robinson 1994
function q = damage_efficiency(E,Z,M)
  ah = 0.5292e-8; % Bohr radius in cm
  e2 = 1.4399e-7; % e^2 in eV-cm
  mpme = 1836.15;
  a12 = ah*(9*pi^2/2^1.5/128/Z)^(1/3);
  El = 2*Z^2*e2/a12;
  kl = (32/3/pi)*sqrt(1/mpme/M)*2^(3/4)*Z^(2/3);
  q = E/El;
  q = q + 0.40244*q.^(3/4) + 3.4008*q.^(1/6);
  q = 1./(1+kl*q);
end

i = find(v>1);
x=logspace(2,log10(max(T)),101);
figure 2
semilogx(T(i),Tdam(i)./T(i),'.',x,damage_efficiency(x,26,55.85))
xlabel('Recoil energy T (eV)');
ylabel('Damage efficiency (T_{dam}/T)');
legend('SRIM','Lindhard approx')
title(testcases{opt})

% b,c for Fe (Nordlund et al)
b = -0.568;
c = 0.286;
xi = (1-c)*(v.^b)+c;
figure 3
loglog(Tdam,v,'.',Tdam,v.*xi,'.')
xlabel('Damage energy T (eV)');
ylabel('# of Vacancies, \nu')
legend('NRT','ARC-DPA','location','northwest')
title(testcases{opt})

% from COLLISON.txt
arc = mean(v.*xi);
% from VACANCY.txt
nrt = sum(A.Vi+A.Vr)/sum(A.Vi);
% approx. arc-dpa calc from https://arxiv.org/abs/2307.12867
arc1 = (1-c)*nrt.^(0.56*(1+b)) + c*nrt;
disp(['Example: ' testcases{opt}])
disp(['NRT     disp/PKA = ' num2str(nrt,3)])
disp(['arc-dpa disp/PKA = ' num2str(arc,3)])
disp(['Approx arc-dpa   = ' num2str(arc1,3)])
disp(['Diff in arc (%)  = ' num2str((arc1/arc-1)*100,3)])
