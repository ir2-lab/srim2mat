====== pysrim run =======
        SRIM-2013.00
==========================================
    Ion and Target VACANCY production     
  See SRIM Outputs\TDATA.txt for calc. details
==========================================
====== TRIM Calc.=  H(5 MeV) ==> FeFoil(  50 um) =============================
------------------------------------------------------------------------------
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
 See file :  SRIM Outputs\TDATA.txt for calculation data
 Ion    =  H   Energy = 5000 keV
============= TARGET MATERIAL ======================================
Layer  1 : FeFoil
Layer Width =     5.E+05 A ;
  Layer # 1- Density = 8.481E22 atoms/cm3 = 7.865 g/cm3
  Layer # 1- Fe = 100  Atomic Percent = 100  Mass Percent
====================================================================
 Total Ions calculated =99999.43
 Total Target Vacancies     = 6 /Ion

==========================================================
  Table Units are  >>>> Vacancies/(Angstrom-Ion)  <<<<  
==========================================================
   TARGET     VACANCIES     VACANCIES 
   DEPTH         by            by     
   (Ang.)       IONS         RECOILS  
-----------  -----------  ------------
500001.E-02  4280.02E-10  4539.40E-10  
100000.E-01  1548.01E-09  3347.60E-09  
150000.E-01  2834.02E-09  6637.44E-09  
200000.E-01  2814.02E-09  6891.08E-09  
250000.E-01  2150.01E-09  4256.72E-09  
300000.E-01  2744.02E-09  8918.93E-09  
350000.E-01  2862.02E-09  8847.05E-09  
400000.E-01  3102.02E-09  6832.44E-09  
450000.E-01  3046.02E-09  5288.89E-09  
500000.E-01  2974.02E-09  3759.09E-09  
550000.E-01  2970.02E-09  5997.50E-09  
600000.E-01  2990.02E-09  7869.19E-09  
650000.E-01  3062.02E-09  9405.74E-09  
700000.E-01  3284.02E-09  9999.75E-09  
750000.E-01  3086.02E-09  6037.39E-09  
800000.E-01  2992.02E-09  7000.90E-09  
850000.E-01  3132.02E-09  5503.47E-09  
900000.E-01  3254.02E-09  8267.19E-09  
950000.E-01  3110.02E-09  8287.94E-09  
100000.E+00  3316.02E-09  8268.39E-09  
105000.E+00  3104.02E-09  1354.12E-08  
110000.E+00  3134.02E-09  4279.15E-09  
115000.E+00  3118.02E-09  9580.63E-09  
120000.E+00  3230.02E-09  9528.99E-09  
125000.E+00  3160.02E-09  7356.58E-09  
130000.E+00  3214.02E-09  6191.57E-09  
135000.E+00  3314.02E-09  7040.02E-09  
140000.E+00  3408.02E-09  8117.01E-09  
145000.E+00  3388.02E-09  7830.35E-09  
150000.E+00  3324.02E-09  8114.81E-09  
155000.E+00  3276.02E-09  5948.54E-09  
160000.E+00  3424.02E-09  7309.16E-09  
165000.E+00  3274.02E-09  5812.74E-09  
170000.E+00  3286.02E-09  1071.50E-08  
175000.E+00  3394.02E-09  5580.01E-09  
180000.E+00  3248.02E-09  8396.13E-09  
185000.E+00  3388.02E-09  5654.81E-09  
190000.E+00  3480.02E-09  8438.76E-09  
195000.E+00  3460.02E-09  9023.97E-09  
200000.E+00  3484.02E-09  8170.22E-09  
205000.E+00  3468.02E-09  5456.72E-09  
210000.E+00  3638.02E-09  1136.16E-08  
215000.E+00  3626.02E-09  7772.91E-09  
220000.E+00  3512.02E-09  1161.48E-08  
225000.E+00  3522.02E-09  6229.77E-09  
230000.E+00  3662.02E-09  8253.85E-09  
235000.E+00  3644.02E-09  8303.65E-09  
240000.E+00  3696.02E-09  9132.78E-09  
245000.E+00  3610.02E-09  5778.37E-09  
250000.E+00  3874.02E-09  1054.99E-08  
255000.E+00  3736.02E-09  1009.78E-08  
260000.E+00  4014.02E-09  8366.17E-09  
265000.E+00  3864.02E-09  1279.66E-08  
270000.E+00  3788.02E-09  6441.45E-09  
275000.E+00  3726.02E-09  1214.02E-08  
280000.E+00  3714.02E-09  6373.65E-09  
285000.E+00  3780.02E-09  8253.61E-09  
290000.E+00  3872.02E-09  7322.75E-09  
295000.E+00  3886.02E-09  8308.63E-09  
300000.E+00  3940.02E-09  9164.97E-09  
305000.E+00  3842.02E-09  8936.10E-09  
310000.E+00  3866.02E-09  1201.70E-08  
315000.E+00  3986.02E-09  6010.80E-09  
320000.E+00  4132.02E-09  1056.49E-08  
325000.E+00  4064.02E-09  9554.89E-09  
330000.E+00  4046.02E-09  9032.14E-09  
335000.E+00  4058.02E-09  1372.65E-08  
340000.E+00  4134.02E-09  9929.99E-09  
345000.E+00  4256.02E-09  9202.55E-09  
350000.E+00  4158.02E-09  1524.58E-08  
355000.E+00  3798.02E-09  1073.75E-08  
360000.E+00  4136.02E-09  8372.56E-09  
365000.E+00  4224.02E-09  1068.19E-08  
370000.E+00  4252.02E-09  8755.61E-09  
375000.E+00  4420.02E-09  1100.66E-08  
380000.E+00  4404.02E-09  1242.90E-08  
385000.E+00  4214.02E-09  1315.02E-08  
390000.E+00  4372.02E-09  9316.97E-09  
395000.E+00  4448.03E-09  9174.35E-09  
400000.E+00  4392.02E-09  8686.87E-09  
405000.E+00  4576.03E-09  1016.42E-08  
410000.E+00  4558.03E-09  8473.61E-09  
415000.E+00  4676.03E-09  1126.20E-08  
420000.E+00  4632.03E-09  8953.41E-09  
425000.E+00  4704.03E-09  6463.73E-09  
430000.E+00  4672.03E-09  1006.12E-08  
435000.E+00  4686.03E-09  9950.93E-09  
440000.E+00  4642.03E-09  1283.19E-08  
445000.E+00  4926.03E-09  1082.64E-08  
450000.E+00  5064.03E-09  1264.26E-08  
455000.E+00  4880.03E-09  1146.49E-08  
460000.E+00  4788.03E-09  1188.68E-08  
465000.E+00  4962.03E-09  1111.63E-08  
470000.E+00  4840.03E-09  1065.83E-08  
475000.E+00  5060.03E-09  1241.64E-08  
480000.E+00  5012.03E-09  1310.08E-08  
485000.E+00  5310.03E-09  7242.25E-09  
490000.E+00  5134.03E-09  1032.56E-08  
495000.E+00  6076.03E-09  1758.59E-08  
500000.E+00  1021.21E-08  2366.14E-08  

 To convert to Energy Lost - multiply by Average Binding Energy =  0  eV/Vacancy
