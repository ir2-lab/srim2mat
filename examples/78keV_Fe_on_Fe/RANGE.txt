====== Fe (10) into Layer 1                     =======
        SRIM-2013.00
==================================================
      ION and final RECOIL ATOM Distributions     
 See  SRIM Outputs\TDATA.txt for calculation details
==================================================
--------------------------------------------------------------------
    Recoil/Damage Calculations made with Kinchin-Pease Estimates    
--------------------------------------------------------------------
----------------------------------------------------------------
  There are NO RECOILS unless a full TRIM calculation is made.   
----------------------------------------------------------------
See file :  SRIM Outputs\TDATA.txt   for details of calculation
 Ion    = Fe   Energy = 78.7  keV
============= TARGET MATERIAL ======================================
Layer  1 : Layer 1
Layer Width =     1.E+03 A   Layer # 1- Density = 8.481E22 atoms/cm3 = 7.865 g/cm3
  Layer # 1- Fe = 100  Atomic Percent = 100  Mass Percent
====================================================================
 Total Ions calculated =3000.00
 Ion Average Range =   277.2E+00 A   Straggling =   133.5E+00 A
 Ion Lateral Range =   873.2E-01 A   Straggling =   113.2E+00 A
 Ion Radial  Range =   139.1E+00 A   Straggling =   833.9E-01 A
====================================================================
 Transmitted Ions =;  Backscattered Ions =15
 (These are not included in Skewne- and Kurtosis below.)

 Range Skewne- = 000.4682  &= [-(X-Rp)^3]/[N*Straggle^3]
 Range Kurtosis = 002.9689  &= [-(X-Rp)^4]/[N*Straggle^4]
 Statistical definitions above are those used in VLSI implant modelling.
====================================================================
=================================================================
  Table Distribution Units are >>>  (Atoms/cm3) / (Atoms/cm2)  <<<  
=================================================================
   DEPTH         Fe       Recoil     
   (Ang.)       Ions     Distribution
-----------  ----------  ------------
100100.E-04  2.0000E+04  0.0000E+00
200100.E-04  4.3333E+04  0.0000E+00
300100.E-04  4.6667E+04  0.0000E+00
400100.E-04  7.6667E+04  0.0000E+00
500100.E-04  5.3333E+04  0.0000E+00
600100.E-04  6.6667E+04  0.0000E+00
700100.E-04  9.0000E+04  0.0000E+00
800100.E-04  8.6667E+04  0.0000E+00
900100.E-04  1.0333E+05  0.0000E+00
100010.E-03  1.5000E+05  0.0000E+00
110010.E-03  1.8000E+05  0.0000E+00
120010.E-03  2.1000E+05  0.0000E+00
130010.E-03  2.5667E+05  0.0000E+00
140010.E-03  1.6667E+05  0.0000E+00
150010.E-03  2.4667E+05  0.0000E+00
160010.E-03  2.5667E+05  0.0000E+00
170010.E-03  2.2667E+05  0.0000E+00
180010.E-03  2.0667E+05  0.0000E+00
190010.E-03  2.9667E+05  0.0000E+00
200010.E-03  3.1667E+05  0.0000E+00
210010.E-03  3.2333E+05  0.0000E+00
220010.E-03  2.8000E+05  0.0000E+00
230010.E-03  2.7000E+05  0.0000E+00
240010.E-03  2.8333E+05  0.0000E+00
250010.E-03  3.5333E+05  0.0000E+00
260010.E-03  2.7667E+05  0.0000E+00
270010.E-03  2.6667E+05  0.0000E+00
280010.E-03  2.6333E+05  0.0000E+00
290010.E-03  1.9333E+05  0.0000E+00
300010.E-03  2.7000E+05  0.0000E+00
310010.E-03  2.6000E+05  0.0000E+00
320010.E-03  2.9333E+05  0.0000E+00
330010.E-03  2.4333E+05  0.0000E+00
340010.E-03  2.4333E+05  0.0000E+00
350010.E-03  2.3000E+05  0.0000E+00
360010.E-03  2.7333E+05  0.0000E+00
370010.E-03  2.1667E+05  0.0000E+00
380010.E-03  1.6667E+05  0.0000E+00
390010.E-03  1.2667E+05  0.0000E+00
400010.E-03  1.5667E+05  0.0000E+00
410010.E-03  1.6333E+05  0.0000E+00
420010.E-03  1.9667E+05  0.0000E+00
430010.E-03  1.5667E+05  0.0000E+00
440010.E-03  1.4333E+05  0.0000E+00
450010.E-03  1.2333E+05  0.0000E+00
460010.E-03  1.0000E+05  0.0000E+00
470010.E-03  1.1333E+05  0.0000E+00
480010.E-03  8.0000E+04  0.0000E+00
490010.E-03  7.3333E+04  0.0000E+00
500010.E-03  9.3333E+04  0.0000E+00
510010.E-03  1.1000E+05  0.0000E+00
520010.E-03  5.3333E+04  0.0000E+00
530010.E-03  5.3333E+04  0.0000E+00
540010.E-03  2.3333E+04  0.0000E+00
550010.E-03  5.3333E+04  0.0000E+00
560010.E-03  7.6667E+04  0.0000E+00
570010.E-03  2.6667E+04  0.0000E+00
580010.E-03  2.3333E+04  0.0000E+00
590010.E-03  1.3333E+04  0.0000E+00
600010.E-03  1.6667E+04  0.0000E+00
610010.E-03  3.0000E+04  0.0000E+00
620010.E-03  3.3333E+04  0.0000E+00
630010.E-03  1.6667E+04  0.0000E+00
640010.E-03  1.0000E+04  0.0000E+00
650010.E-03  3.3333E+03  0.0000E+00
660010.E-03  1.6667E+04  0.0000E+00
670010.E-03  6.6667E+03  0.0000E+00
680010.E-03  3.3333E+03  0.0000E+00
690010.E-03  1.3333E+04  0.0000E+00
700010.E-03  1.3333E+04  0.0000E+00
710010.E-03  6.6667E+03  0.0000E+00
720010.E-03  6.6667E+03  0.0000E+00
730010.E-03  0.0000E+00  0.0000E+00
740010.E-03  0.0000E+00  0.0000E+00
750010.E-03  3.3333E+03  0.0000E+00
760010.E-03  0.0000E+00  0.0000E+00
770010.E-03  0.0000E+00  0.0000E+00
780010.E-03  0.0000E+00  0.0000E+00
790010.E-03  0.0000E+00  0.0000E+00
800010.E-03  0.0000E+00  0.0000E+00
810010.E-03  0.0000E+00  0.0000E+00
820010.E-03  0.0000E+00  0.0000E+00
830010.E-03  0.0000E+00  0.0000E+00
840010.E-03  0.0000E+00  0.0000E+00
850010.E-03  0.0000E+00  0.0000E+00
860010.E-03  3.3333E+03  0.0000E+00
870010.E-03  0.0000E+00  0.0000E+00
880010.E-03  0.0000E+00  0.0000E+00
890010.E-03  0.0000E+00  0.0000E+00
900010.E-03  0.0000E+00  0.0000E+00
910010.E-03  0.0000E+00  0.0000E+00
920010.E-03  0.0000E+00  0.0000E+00
930010.E-03  0.0000E+00  0.0000E+00
940010.E-03  0.0000E+00  0.0000E+00
950010.E-03  0.0000E+00  0.0000E+00
960010.E-03  0.0000E+00  0.0000E+00
970010.E-03  0.0000E+00  0.0000E+00
980010.E-03  0.0000E+00  0.0000E+00
990010.E-03  0.0000E+00  0.0000E+00
100001.E-02  0.0000E+00  0.0000E+00
