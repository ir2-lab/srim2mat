%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Load SRIM data and plot them
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear
addpath('../m');

testcases = { ...
  '1 MeV H ions on Fe target',...
  '5 MeV H ions on Fe target',...
  '2 MeV Fe ions on Fe target'...
  '78 keV Fe ions on Fe target'};
  
testfolders = { ...
  './1MeV_H_on_Fe', ...
  './5MeV_H_on_Fe', ...
  './2MeV_Fe_on_Fe',...
  './78keV_Fe_on_Fe'};
  
opt = menu('Choose SRIM test case', testcases);
if opt==0, return; end

data = srim2mat(testfolders{opt});
titlestr = testcases{opt};

figure 1
clf 
plot(data.x,[data.Vi data.Vr])
xlabel('x (A)')
ylabel('Vacancies / ion-A')
legend('ions','recoils')
title([titlestr ' - VACANCY.txt']);

figure 2
clf
plot(data.x,[data.Ri data.Rr])
xlabel('x (A)')
ylabel('(atoms/cm^3)/(atoms/cm^2)')
legend('ions','recoils')
title([titlestr ' - RANGE.txt'])

figure 3
clf
plot(data.x,[data.ERi data.ERr])
xlabel('x (A)')
ylabel('eV /(Angstrom-Ion)')
legend('ions','recoils')
title([titlestr ' - E2RECOIL.txt'])

figure 4
clf
plot(data.x,[data.EIi data.EIr])
xlabel('x (A)')
ylabel('eV /(Angstrom-Ion)')
legend('ions','recoils')
title([titlestr ' - IONIZ.txt'])

figure 5
clf
plot(data.x,[data.EPi data.EPr])
xlabel('x (A)')
ylabel('eV /(Angstrom-Ion)')
legend('ions','recoils')
title([titlestr ' - PHONON.txt'])