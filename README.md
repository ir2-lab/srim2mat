# srim2mat

Load SRIM output data into MATLAB / OCTAVE.

The [SRIM (http://srim.org/)](http://srim.org/) ion transport simulation code generates a number of output text files (``'VACANCY.txt'``, ``'RANGE.txt'``, ...), which contain the various simulation results.

``srim2mat`` facilitates loading all SRIM data into MATLAB / OCTAVE.
The command
```
data = srim2mat( srim_output_folder );
```
reads all SRIM files in the specified folder and returns a MATLAB structure (``data``) where the SRIM tables are stored as vectors. The files must have the original names as written out by SRIM.

A special case is the output file ``COLLISON.txt``, where ion collision events are stored. It is typically a very large file and needs special handling. Thus, a separate function is available for loading the data:
```
data = collison2mat( srim_output_folder );
```
This generates the ``N x 8`` matrix ``data``, where ``N`` is the number of collision events. 

The data can be stored in a binary file for future use, e.g.
```matlab
folder = '/path/to/srim/output';
srim_out = srim2mat( folder );
collison_dat = collison2mat( folder );
save srim_results.dat srim_out collison_dat
```

## Usage

### srim2mat

```matlab
function out = srim2mat(folder)
% function out = read_srim_output(folder)
%
% Parses SRIM output files from folder and returns data in
% structure out
%
% Files parsed: VACANCY.txt, IONIZ.txt, PHONON.txt, E2RECOIL.txt,
%               RANGE.txt, NOVAC.txt
%
% If a file does not exist the corresponding data remain empty
%
% The structure out has the following fields:
%   x   :  target depth (Angstroem)
%   Vi  :  vacancies from ions (Atoms/ion-Angstroem) 
%   Vr  :  vacancies from recoils (Atoms/ion-Angstroem)
%   RC  :  replacement collisions (Atoms/ion-Angstroem)
%   EIi :  ionization energy by ions (eV/ion-Angstroem)
%   EIr :  ionization energy by target recoils (eV/ion-Angstroem)
%   EPi :  phonon energy by ions (eV/ion-Angstroem)
%   EPr :  phonon energy by target recoils (eV/ion-Angstroem)
%   ERi :  recoil energy from ions (eV/ion-Angstroem)
%   ERr :  recoil energy absorbed by target recoils (eV/ion-Angstroem)
%   Ri  :  range distribution of ions (atoms/cm3) / (atoms/cm2)
%   Rr  :  range distribution of recoils (atoms/cm3) / (atoms/cm2)
%   parsedFiles : cell array with all succesfully parsed files
%   failedFiles : cell array with all failed files
```

### collison2mat

```matlab
function data = collison2mat(folder,fnm)
% function data = collison2mat(folder,fnm)
%
% Parses the SRIM output file COLLISON.txt from the specified folder and 
% returns all data in a matrix.
%
% Only files created with the SRIM option "Kinchin-Pease" are supported.
%
% If the original file has been renamed, pass the new name in fnm
%
% Each row of the output matrix data corresponds to an ion collision event.
% The 9 columns store the following information: 
%   1 :  ion number
%   2 :  ion energy (keV) 
%   3 :  Depth (X) (Angstroem)
%   4 :  Lateral distance (Y) (Angstroem)
%   5 :  Lateral distance (Z) (Angstroem)
%   6 :  Electronic stopping Se (eV/Angstroem)
%   7 :  Atomic number of recoil atom
%   8 :  Recoil energy (eV)
%   9 :  Target displacements
```

## Examples

Examples can be found in the ``examples/`` folder.

* [examples/eval_srim.m](examples/eval_srim.m) shows how to load SRIM output files and display/plot basic parameters
* [examples/eval_collison.m](examples/eval_collison.m) show how to handle the COLLISON.txt file.
* [examples/arc-dpa.ipynb](examples/arc-dpa.ipynb) is a jupyter notebook which describes how to use SRIM output to calculate damage according to the arc-dpa model.

## Install

Copy all files from folder ``m/``  to a location in the MATLAB / OCTAVE path (e.g. your working folder).


